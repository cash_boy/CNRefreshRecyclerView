package com.aspsine.irecyclerview;

/**
 * Created by CN.
 */

public interface OnAllListener {
    void onRefresh();
    void onLoadMore();
}
